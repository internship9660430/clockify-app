import { ApiProperty } from '@nestjs/swagger';
import { CreateProfileDto } from './create-profile.dto';

export class ProfileResponseDto extends CreateProfileDto {
  @ApiProperty()
  id: string;
}
