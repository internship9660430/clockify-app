import { ApiProperty } from '@nestjs/swagger';

export class CreateProfileDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  photoLocation: string;

  @ApiProperty()
  language: string;

  @ApiProperty()
  timeZone: string;

  @ApiProperty()
  userId: string;
}
