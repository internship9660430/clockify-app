import { ApiProperty } from '@nestjs/swagger';
import { CreateClientDto } from './create-client.dto';

export class ClientResponseDto extends CreateClientDto {
  @ApiProperty()
  id: string;
}
