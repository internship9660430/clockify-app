import { ApiProperty } from '@nestjs/swagger';
import { CreateProjectDto } from './create-project.dto';

export class ProjectResponseDto extends CreateProjectDto {
  @ApiProperty()
  id: string;
}
