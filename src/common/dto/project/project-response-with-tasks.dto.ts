import { ApiProperty, OmitType } from '@nestjs/swagger';
import { ProjectResponseDto } from './project-response.dto';
import { TaskResponseDto } from '../task/task-response.dto';

class TaskResponseWithoutProjectIdDto extends OmitType(TaskResponseDto, [
  'projectId',
]) {}

export class ProjectResponseWithTasksDto extends ProjectResponseDto {
  @ApiProperty({ type: TaskResponseWithoutProjectIdDto, isArray: true })
  tasks: TaskResponseWithoutProjectIdDto[];
}
