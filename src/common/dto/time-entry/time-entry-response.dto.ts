import { ApiProperty } from '@nestjs/swagger';
import { CreateTimeEntryWithDurationDto } from './create-time-entry-with-duration.dto';

export class TimeEntryResponseDto extends CreateTimeEntryWithDurationDto {
  @ApiProperty()
  id: string;
}
