import { ApiProperty } from '@nestjs/swagger';
import { CreateTimeEntryDto } from './create-time-entry.dto';

export class CreateTimeEntryWithDurationDto extends CreateTimeEntryDto {
  @ApiProperty()
  durationInSecs: number;
}
