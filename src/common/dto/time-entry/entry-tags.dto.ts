import { ApiProperty } from '@nestjs/swagger';

export class EntryTagsDto {
  @ApiProperty()
  tagIds: string[];
}
