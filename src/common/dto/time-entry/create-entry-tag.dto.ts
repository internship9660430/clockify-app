import { ApiProperty } from '@nestjs/swagger';

export class CreateEntryTagDto {
  @ApiProperty()
  timeEntryId: string;

  @ApiProperty()
  tagId: string;
}
