import { ApiProperty, OmitType } from '@nestjs/swagger';
import { TagResponseDto } from '../tag/tag-response.dto';
import { TimeEntryResponseDto } from './time-entry-response.dto';

class PartialTagResponseDto extends OmitType(TagResponseDto, ['workspaceId']) {}

class TagInfoDto {
  @ApiProperty()
  tag: PartialTagResponseDto;
}

export class TimeEntryResponseWithTagsDto extends TimeEntryResponseDto {
  @ApiProperty({ type: TagInfoDto, isArray: true })
  tags: TagInfoDto[];
}
