import { ApiProperty } from '@nestjs/swagger';

export class CreateTimeEntryDto {
  @ApiProperty()
  description: string;

  @ApiProperty()
  startDatetime: string;

  @ApiProperty()
  stopDatetime: string;

  @ApiProperty()
  isBillable: boolean;

  @ApiProperty()
  userId: string;

  @ApiProperty()
  workspaceId: string;

  @ApiProperty()
  projectId: string;

  @ApiProperty()
  taskId: string;
}
