import { ApiProperty } from '@nestjs/swagger';

export class WorkspaceUsersDto {
  @ApiProperty()
  userIds: string[];
}
