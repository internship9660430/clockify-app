import { ApiProperty, OmitType } from '@nestjs/swagger';
import { CreateWorkspaceDto } from './create-workspace.dto';

export class WorkspaceResponseDto extends OmitType(CreateWorkspaceDto, [
  'ownerId',
]) {
  @ApiProperty()
  id: string;
}
