import { ApiProperty } from '@nestjs/swagger';
import { WorkspaceResponseDto } from './workspace-response.dto';
import { UserResponseDto } from '../user/user-response.dto';
import { Role } from '@prisma/client';

class UserWithWorkspaceRoleDto {
  @ApiProperty()
  user: UserResponseDto;

  @ApiProperty()
  role: Role;
}

export class WorkspaceResponseWithUsersDto extends WorkspaceResponseDto {
  @ApiProperty({ type: UserWithWorkspaceRoleDto, isArray: true })
  users: UserWithWorkspaceRoleDto[];
}
