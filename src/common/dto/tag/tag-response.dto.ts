import { ApiProperty } from '@nestjs/swagger';
import { CreateTagDto } from './create-tag.dto';

export class TagResponseDto extends CreateTagDto {
  @ApiProperty()
  id: string;
}
