import { ApiProperty, OmitType } from '@nestjs/swagger';
import { UserResponseDto } from './user-response.dto';
import { ProfileResponseDto } from '../profile/profile-response.dto';

class ProfileResponseWithoutUserIdDto extends OmitType(ProfileResponseDto, [
  'userId',
]) {}

export class UserResponseWithProfileDto extends UserResponseDto {
  @ApiProperty()
  profile: ProfileResponseWithoutUserIdDto;
}
