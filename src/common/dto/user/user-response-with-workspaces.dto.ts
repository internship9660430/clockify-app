import { ApiProperty } from '@nestjs/swagger';
import { UserResponseDto } from './user-response.dto';
import { WorkspaceResponseDto } from '../workspace/workspace-response.dto';
import { Role } from '@prisma/client';

class WorkspaceInfoDto {
  @ApiProperty({ type: WorkspaceResponseDto })
  workspace: WorkspaceResponseDto;

  @ApiProperty()
  role: Role;
}

export class UserResponseWithWorkspacesDto extends UserResponseDto {
  @ApiProperty({ type: WorkspaceInfoDto, isArray: true })
  workspaces: WorkspaceInfoDto[];
}
