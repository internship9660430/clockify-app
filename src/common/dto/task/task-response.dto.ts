import { ApiProperty } from '@nestjs/swagger';
import { CreateTaskDto } from './create-task.dto';

export class TaskResponseDto extends CreateTaskDto {
  @ApiProperty()
  id: string;
}
