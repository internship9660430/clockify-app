import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { WorkspaceService } from './workspace.service';
import { CreateWorkspaceDto } from '../common/dto/workspace/create-workspace.dto';
import { UpdateWorkspaceDto } from '../common/dto/workspace/update-workspace.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { WorkspaceResponseDto } from 'src/common/dto/workspace/workspace-response.dto';
import { WorkspaceResponseWithUsersDto } from 'src/common/dto/workspace/workspace-response-with-users.dto';
import { WorkspaceUsersDto } from 'src/common/dto/workspace/workspace-users.dto';

@Controller('workspaces')
@ApiTags('workspaces')
export class WorkspaceController {
  constructor(private readonly workspaceService: WorkspaceService) {}

  @Post()
  @ApiCreatedResponse({ type: WorkspaceResponseDto })
  create(@Body() createWorkspaceDto: CreateWorkspaceDto) {
    return this.workspaceService.create(createWorkspaceDto);
  }

  @Get()
  @ApiOkResponse({ type: WorkspaceResponseDto, isArray: true })
  findAll() {
    return this.workspaceService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: WorkspaceResponseDto })
  findOne(@Param('id') id: string) {
    return this.workspaceService.findOne(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: WorkspaceResponseDto })
  update(
    @Param('id') id: string,
    @Body() updateWorkspaceDto: UpdateWorkspaceDto,
  ) {
    return this.workspaceService.update(id, updateWorkspaceDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: WorkspaceResponseDto })
  remove(@Param('id') id: string) {
    return this.workspaceService.remove(id);
  }

  @Get(':id/owner')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  findOneWithOwner(@Param('id') id: string) {
    return this.workspaceService.findOneWithOwner(id);
  }

  @Post(':id/admins')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  createAdmins(
    @Param('id') id: string,
    @Body() workspaceUsersDto: WorkspaceUsersDto,
  ) {
    return this.workspaceService.createAdmins(id, workspaceUsersDto);
  }

  @Get(':id/admins')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  findOneWithAdmins(@Param('id') id: string) {
    return this.workspaceService.findOneWithAdmins(id);
  }

  @Delete(':id/admins')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  removeAdmins(
    @Param('id') id: string,
    @Body() workspaceUsersDto: WorkspaceUsersDto,
  ) {
    return this.workspaceService.removeAdmins(id, workspaceUsersDto);
  }

  @Post(':id/members')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  createMembers(
    @Param('id') id: string,
    @Body() workspaceUsersDto: WorkspaceUsersDto,
  ) {
    return this.workspaceService.createMembers(id, workspaceUsersDto);
  }

  @Get(':id/members')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  findOneWithMembers(@Param('id') id: string) {
    return this.workspaceService.findOneWithMembers(id);
  }

  @Delete(':id/members')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  removeMembers(
    @Param('id') id: string,
    @Body() workspaceUsersDto: WorkspaceUsersDto,
  ) {
    return this.workspaceService.removeMembers(id, workspaceUsersDto);
  }

  @Get(':id/users')
  @ApiOkResponse({ type: WorkspaceResponseWithUsersDto })
  findOneWithUsers(@Param('id') id: string) {
    return this.workspaceService.findOneWithUsers(id);
  }

  @Get(':id/users/:userId/time-entries')
  @ApiOkResponse()
  findOneWithUserTimeEntries(
    @Param('id') id: string,
    @Param('userId') userId: string,
  ) {
    return this.workspaceService.findOneWithUserTimeEntries(id, userId);
  }

  @Get(':id/time-entries')
  @ApiOkResponse()
  findOneWithTimeEntries(@Param('id') id: string) {
    return this.workspaceService.findOneWithTimeEntries(id);
  }

  @Get(':id/projects')
  @ApiOkResponse()
  findOneWithProjects(@Param('id') id: string) {
    return this.workspaceService.findOneWithProjects(id);
  }

  @Get(':id/clients')
  @ApiOkResponse()
  findOneWithClients(@Param('id') id: string) {
    return this.workspaceService.findOneWithClients(id);
  }

  @Get(':id/tags')
  @ApiOkResponse()
  findOneWithTags(@Param('id') id: string) {
    return this.workspaceService.findOneWithTags(id);
  }
}
