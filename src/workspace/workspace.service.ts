import { Injectable } from '@nestjs/common';
import { CreateWorkspaceDto } from '../common/dto/workspace/create-workspace.dto';
import { UpdateWorkspaceDto } from '../common/dto/workspace/update-workspace.dto';
import { WorkspaceDatabaseService } from 'src/database/workspace-database.service';
import { WorkspaceUsersDto } from 'src/common/dto/workspace/workspace-users.dto';
import { UserWorkspaceDatabaseService } from 'src/database/user-workspace-database.service';
import { CreateUserWorkspaceDto } from 'src/common/dto/workspace/create-user-workspace.dto';
import { Role } from '@prisma/client';

@Injectable()
export class WorkspaceService {
  constructor(
    private readonly workspaceDatabaseService: WorkspaceDatabaseService,
    private readonly userWorkspaceDatabaseService: UserWorkspaceDatabaseService,
  ) {}

  create(createWorkspaceDto: CreateWorkspaceDto) {
    return this.workspaceDatabaseService.create(createWorkspaceDto);
  }

  findAll() {
    return this.workspaceDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.workspaceDatabaseService.findOne(id);
  }

  update(id: string, updateWorkspaceDto: UpdateWorkspaceDto) {
    return this.workspaceDatabaseService.update(id, updateWorkspaceDto);
  }

  remove(id: string) {
    return this.workspaceDatabaseService.remove(id);
  }

  findOneWithOwner(id: string) {
    return this.workspaceDatabaseService.findOneWithOwner(id);
  }

  async createAdmins(
    workspaceId: string,
    workspaceUsersDto: WorkspaceUsersDto,
  ) {
    const promises = workspaceUsersDto.userIds.map(async (userId) => {
      const userWorkspace =
        await this.userWorkspaceDatabaseService.findWithUserAndWorkspaceIds(
          userId,
          workspaceId,
        );

      if (userWorkspace) {
        return await this.userWorkspaceDatabaseService.update(
          userWorkspace.id,
          { role: Role.ADMIN },
        );
      } else {
        const createUserWorkspaceDto: CreateUserWorkspaceDto = {
          userId,
          workspaceId,
          role: Role.ADMIN,
        };

        return await this.userWorkspaceDatabaseService.create(
          createUserWorkspaceDto,
        );
      }
    });

    await Promise.all(promises);

    return this.workspaceDatabaseService.findOneWithAdmins(workspaceId);
  }

  findOneWithAdmins(id: string) {
    return this.workspaceDatabaseService.findOneWithAdmins(id);
  }

  async removeAdmins(
    workspaceId: string,
    workspaceUsersDto: WorkspaceUsersDto,
  ) {
    await this.userWorkspaceDatabaseService.removeAdmins(
      workspaceId,
      workspaceUsersDto,
    );

    return this.workspaceDatabaseService.findOneWithAdmins(workspaceId);
  }

  async createMembers(
    workspaceId: string,
    workspaceUsersDto: WorkspaceUsersDto,
  ) {
    const promises = workspaceUsersDto.userIds.map(async (userId) => {
      const createUserWorkspaceDto: CreateUserWorkspaceDto = {
        userId,
        workspaceId,
        role: Role.MEMBER,
      };

      return await this.userWorkspaceDatabaseService.create(
        createUserWorkspaceDto,
      );
    });

    await Promise.all(promises);

    return this.workspaceDatabaseService.findOneWithMembers(workspaceId);
  }

  findOneWithMembers(id: string) {
    return this.workspaceDatabaseService.findOneWithMembers(id);
  }

  async removeMembers(
    workspaceId: string,
    workspaceUsersDto: WorkspaceUsersDto,
  ) {
    const promises = workspaceUsersDto.userIds.map(async (userId) => {
      return await this.userWorkspaceDatabaseService.removeMember(
        workspaceId,
        userId,
      );
    });

    await Promise.all(promises);

    return this.workspaceDatabaseService.findOneWithMembers(workspaceId);
  }

  findOneWithUsers(id: string) {
    return this.workspaceDatabaseService.findOneWithUsers(id);
  }

  findOneWithUserTimeEntries(id: string, userId: string) {
    return this.workspaceDatabaseService.findOneWithUserTimeEntries(id, userId);
  }

  findOneWithTimeEntries(id: string) {
    return this.workspaceDatabaseService.findOneWithTimeEntries(id);
  }

  findOneWithProjects(id: string) {
    return this.workspaceDatabaseService.findOneWithProjects(id);
  }

  findOneWithClients(id: string) {
    return this.workspaceDatabaseService.findOneWithClients(id);
  }

  findOneWithTags(id: string) {
    return this.workspaceDatabaseService.findOneWithTags(id);
  }
}
