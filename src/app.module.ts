import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { UserModule } from './user/user.module';
import { ProfileModule } from './profile/profile.module';
import { WorkspaceModule } from './workspace/workspace.module';
import { TimeEntryModule } from './time-entry/time-entry.module';
import { ProjectModule } from './project/project.module';
import { TaskModule } from './task/task.module';
import { ClientModule } from './client/client.module';
import { TagModule } from './tag/tag.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [
    PrismaModule,
    UserModule,
    ProfileModule,
    WorkspaceModule,
    TimeEntryModule,
    ProjectModule,
    TaskModule,
    ClientModule,
    TagModule,
    DatabaseModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
