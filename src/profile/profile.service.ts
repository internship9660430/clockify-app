import { Injectable } from '@nestjs/common';
import { CreateProfileDto } from 'src/common/dto/profile/create-profile.dto';
import { UpdateProfileDto } from 'src/common/dto/profile/update-profile.dto';
import { ProfileDatabaseService } from 'src/database/profile-database.service';

@Injectable()
export class ProfileService {
  constructor(
    private readonly profileDatabaseService: ProfileDatabaseService,
  ) {}

  create(createProfileDto: CreateProfileDto) {
    return this.profileDatabaseService.create(createProfileDto);
  }

  findAll() {
    return this.profileDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.profileDatabaseService.findOne(id);
  }

  update(id: string, updateProfileDto: UpdateProfileDto) {
    return this.profileDatabaseService.update(id, updateProfileDto);
  }

  remove(id: string) {
    return this.profileDatabaseService.remove(id);
  }
}
