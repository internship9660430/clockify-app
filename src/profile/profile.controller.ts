import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ProfileService } from './profile.service';
import { CreateProfileDto } from 'src/common/dto/profile/create-profile.dto';
import { UpdateProfileDto } from 'src/common/dto/profile/update-profile.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ProfileResponseDto } from 'src/common/dto/profile/profile-response.dto';

@Controller('profiles')
@ApiTags('profiles')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Post()
  @ApiCreatedResponse({ type: ProfileResponseDto })
  create(@Body() createProfileDto: CreateProfileDto) {
    return this.profileService.create(createProfileDto);
  }

  @Get()
  @ApiOkResponse({ type: ProfileResponseDto, isArray: true })
  findAll() {
    return this.profileService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: ProfileResponseDto })
  findOne(@Param('id') id: string) {
    return this.profileService.findOne(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: ProfileResponseDto })
  update(@Param('id') id: string, @Body() updateProfileDto: UpdateProfileDto) {
    return this.profileService.update(id, updateProfileDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: ProfileResponseDto })
  remove(@Param('id') id: string) {
    return this.profileService.remove(id);
  }
}
