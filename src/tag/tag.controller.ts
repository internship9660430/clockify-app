import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TagService } from './tag.service';
import { CreateTagDto } from '../common/dto/tag/create-tag.dto';
import { UpdateTagDto } from '../common/dto/tag/update-tag.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { TagResponseDto } from 'src/common/dto/tag/tag-response.dto';

@Controller('tags')
@ApiTags('tags')
export class TagController {
  constructor(private readonly tagService: TagService) {}

  @Post()
  @ApiCreatedResponse({ type: TagResponseDto })
  create(@Body() createTagDto: CreateTagDto) {
    return this.tagService.create(createTagDto);
  }

  @Get()
  @ApiOkResponse({ type: TagResponseDto, isArray: true })
  findAll() {
    return this.tagService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: TagResponseDto })
  findOne(@Param('id') id: string) {
    return this.tagService.findOne(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: TagResponseDto })
  update(@Param('id') id: string, @Body() updateTagDto: UpdateTagDto) {
    return this.tagService.update(id, updateTagDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: TagResponseDto })
  remove(@Param('id') id: string) {
    return this.tagService.remove(id);
  }
}
