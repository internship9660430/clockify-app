import { Injectable } from '@nestjs/common';
import { CreateTagDto } from '../common/dto/tag/create-tag.dto';
import { UpdateTagDto } from '../common/dto/tag/update-tag.dto';
import { TagDatabaseService } from 'src/database/tag-database.service';

@Injectable()
export class TagService {
  constructor(private readonly tagDatabaseService: TagDatabaseService) {}

  create(createTagDto: CreateTagDto) {
    return this.tagDatabaseService.create(createTagDto);
  }

  findAll() {
    return this.tagDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.tagDatabaseService.findOne(id);
  }

  update(id: string, updateTagDto: UpdateTagDto) {
    return this.tagDatabaseService.update(id, updateTagDto);
  }

  remove(id: string) {
    return this.tagDatabaseService.remove(id);
  }
}
