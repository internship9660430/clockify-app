import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from '../common/dto/task/create-task.dto';
import { UpdateTaskDto } from '../common/dto/task/update-task.dto';
import { TaskDatabaseService } from 'src/database/task-database.service';

@Injectable()
export class TaskService {
  constructor(private readonly taskDatabaseService: TaskDatabaseService) {}

  create(createTaskDto: CreateTaskDto) {
    return this.taskDatabaseService.create(createTaskDto);
  }

  findAll() {
    return this.taskDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.taskDatabaseService.findOne(id);
  }

  update(id: string, updateTaskDto: UpdateTaskDto) {
    return this.taskDatabaseService.update(id, updateTaskDto);
  }

  remove(id: string) {
    return this.taskDatabaseService.remove(id);
  }
}
