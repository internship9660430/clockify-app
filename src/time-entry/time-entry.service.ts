import { EntryTagDatabaseService } from './../database/entry-tag-database.service';
import { Injectable } from '@nestjs/common';
import { CreateTimeEntryDto } from '../common/dto/time-entry/create-time-entry.dto';
import { UpdateTimeEntryDto } from '../common/dto/time-entry/update-time-entry.dto';
import { TimeEntryDatabaseService } from 'src/database/time-entry-database.service';
import { EntryTagsDto } from 'src/common/dto/time-entry/entry-tags.dto';
import { CreateEntryTagDto } from 'src/common/dto/time-entry/create-entry-tag.dto';
import { CreateTimeEntryWithDurationDto } from 'src/common/dto/time-entry/create-time-entry-with-duration.dto';
import { differenceInSeconds, parseISO } from 'date-fns';

@Injectable()
export class TimeEntryService {
  constructor(
    private readonly timeEntryDatabaseService: TimeEntryDatabaseService,
    private readonly entryTagDatabaseService: EntryTagDatabaseService,
  ) {}

  create(createTimeEntryDto: CreateTimeEntryDto) {
    const durationInSecs = differenceInSeconds(
      parseISO(createTimeEntryDto.stopDatetime),
      parseISO(createTimeEntryDto.startDatetime),
    );

    const createTimeEntryWithDurationDto: CreateTimeEntryWithDurationDto = {
      ...createTimeEntryDto,
      durationInSecs,
    };

    return this.timeEntryDatabaseService.create(createTimeEntryWithDurationDto);
  }

  findAll() {
    return this.timeEntryDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.timeEntryDatabaseService.findOne(id);
  }

  update(id: string, updateTimeEntryDto: UpdateTimeEntryDto) {
    return this.timeEntryDatabaseService.update(id, updateTimeEntryDto);
  }

  remove(id: string) {
    return this.timeEntryDatabaseService.remove(id);
  }

  async createTags(timeEntryId: string, entryTagsDto: EntryTagsDto) {
    const promises = entryTagsDto.tagIds.map(async (tagId) => {
      const createEntryTagDto: CreateEntryTagDto = {
        timeEntryId,
        tagId,
      };

      return await this.entryTagDatabaseService.create(createEntryTagDto);
    });

    await Promise.all(promises);

    return this.findOneWithTags(timeEntryId);
  }

  findOneWithTags(id: string) {
    return this.timeEntryDatabaseService.findOneWithTags(id);
  }

  async removeTags(timeEntryId: string, entryTagsDto: EntryTagsDto) {
    const promises = entryTagsDto.tagIds.map(async (tagId) => {
      return await this.entryTagDatabaseService.remove(timeEntryId, tagId);
    });

    await Promise.all(promises);

    return this.findOneWithTags(timeEntryId);
  }
}
