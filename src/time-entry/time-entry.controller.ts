import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TimeEntryService } from './time-entry.service';
import { CreateTimeEntryDto } from '../common/dto/time-entry/create-time-entry.dto';
import { UpdateTimeEntryDto } from '../common/dto/time-entry/update-time-entry.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { TimeEntryResponseDto } from 'src/common/dto/time-entry/time-entry-response.dto';
import { EntryTagsDto } from 'src/common/dto/time-entry/entry-tags.dto';
import { TimeEntryResponseWithTagsDto } from 'src/common/dto/time-entry/time-entry-response-with-tags.dto';

@Controller('time-entries')
@ApiTags('time-entries')
export class TimeEntryController {
  constructor(private readonly timeEntryService: TimeEntryService) {}

  @Post()
  @ApiCreatedResponse({ type: TimeEntryResponseDto })
  create(@Body() createTimeEntryDto: CreateTimeEntryDto) {
    return this.timeEntryService.create(createTimeEntryDto);
  }

  @Get()
  @ApiOkResponse({ type: TimeEntryResponseDto, isArray: true })
  findAll() {
    return this.timeEntryService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: TimeEntryResponseDto })
  findOne(@Param('id') id: string) {
    return this.timeEntryService.findOne(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: TimeEntryResponseDto })
  update(
    @Param('id') id: string,
    @Body() updateTimeEntryDto: UpdateTimeEntryDto,
  ) {
    return this.timeEntryService.update(id, updateTimeEntryDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: TimeEntryResponseDto })
  remove(@Param('id') id: string) {
    return this.timeEntryService.remove(id);
  }

  @Post(':id/tags')
  @ApiCreatedResponse({ type: TimeEntryResponseWithTagsDto })
  createTags(@Param('id') id: string, @Body() entryTagsDto: EntryTagsDto) {
    return this.timeEntryService.createTags(id, entryTagsDto);
  }

  @Get(':id/tags')
  @ApiOkResponse({ type: TimeEntryResponseWithTagsDto })
  findOneWithTags(@Param('id') id: string) {
    return this.timeEntryService.findOneWithTags(id);
  }

  @Delete(':id/tags')
  @ApiOkResponse({ type: TimeEntryResponseWithTagsDto })
  removeTags(@Param('id') id: string, @Body() entryTagsDto: EntryTagsDto) {
    return this.timeEntryService.removeTags(id, entryTagsDto);
  }
}
