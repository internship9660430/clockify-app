import { TimeEntry as TimeEntryModel } from '@prisma/client';

export class TimeEntryEntity implements TimeEntryModel {
  id: string;
  description: string;
  startDatetime: Date;
  stopDatetime: Date;
  durationInSecs: number;
  isBillable: boolean;
  userId: string;
  workspaceId: string;
  projectId: string;
  taskId: string;
}
