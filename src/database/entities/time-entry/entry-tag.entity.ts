import { EntryTag as EntryTagModel } from '@prisma/client';

export class EntryTagEntity implements EntryTagModel {
  id: string;
  timeEntryId: string;
  tagId: string;
}
