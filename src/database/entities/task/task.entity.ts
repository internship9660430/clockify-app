import { Task as TaskModel } from '@prisma/client';

export class TaskEntity implements TaskModel {
  id: string;
  name: string;
  isActive: boolean;
  projectId: string;
}
