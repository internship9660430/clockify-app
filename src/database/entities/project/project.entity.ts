import { Project as ProjectModel } from '@prisma/client';

export class ProjectEntity implements ProjectModel {
  id: string;
  name: string;
  color: string;
  isActive: boolean;
  workspaceId: string;
  clientId: string;
}
