import { Client as ClientModel } from '@prisma/client';

export class ClientEntity implements ClientModel {
  id: string;
  name: string;
  workspaceId: string;
}
