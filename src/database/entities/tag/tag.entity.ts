import { Tag as TagModel } from '@prisma/client';

export class TagEntity implements TagModel {
  id: string;
  name: string;
  isActive: boolean;
  workspaceId: string;
}
