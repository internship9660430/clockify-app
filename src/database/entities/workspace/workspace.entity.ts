import { Workspace as WorkspaceModel } from '@prisma/client';

export class WorkspaceEntity implements WorkspaceModel {
  id: string;
  name: string;
}
