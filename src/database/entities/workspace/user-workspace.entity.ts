import { $Enums, UserWorkspace as UserWorkspaceModel } from '@prisma/client';

export class UserWorkspaceEntity implements UserWorkspaceModel {
  id: string;
  userId: string;
  workspaceId: string;
  role: $Enums.Role;
}
