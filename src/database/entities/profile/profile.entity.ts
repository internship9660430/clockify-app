import { Profile as ProfileModel } from '@prisma/client';

export class ProfileEntity implements ProfileModel {
  id: string;
  name: string;
  photoLocation: string;
  language: string;
  timeZone: string;
  userId: string;
}
