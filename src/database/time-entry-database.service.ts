import { Injectable } from '@nestjs/common';
import { UpdateTimeEntryDto } from '../common/dto/time-entry/update-time-entry.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { TimeEntryResponseDto } from 'src/common/dto/time-entry/time-entry-response.dto';
import { TimeEntryEntity } from './entities/time-entry/time-entry.entity';
import { CreateTimeEntryWithDurationDto } from 'src/common/dto/time-entry/create-time-entry-with-duration.dto';

@Injectable()
export class TimeEntryDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToTimeEntryResponseDto(
    timeEntryEntity: TimeEntryEntity,
  ): TimeEntryResponseDto {
    const timeEntryResponse: TimeEntryResponseDto = {
      id: timeEntryEntity.id,
      description: timeEntryEntity.description,
      startDatetime: timeEntryEntity.startDatetime.toISOString(),
      stopDatetime: timeEntryEntity.stopDatetime.toISOString(),
      durationInSecs: timeEntryEntity.durationInSecs,
      isBillable: timeEntryEntity.isBillable,
      userId: timeEntryEntity.userId,
      workspaceId: timeEntryEntity.workspaceId,
      projectId: timeEntryEntity.projectId,
      taskId: timeEntryEntity.taskId,
    };

    return timeEntryResponse;
  }

  async create(
    createTimeEntryWithDurationDto: CreateTimeEntryWithDurationDto,
  ): Promise<TimeEntryResponseDto | null> {
    const timeEntry = await this.prisma.timeEntry.create({
      data: createTimeEntryWithDurationDto,
    });
    return this.convertToTimeEntryResponseDto(timeEntry);
  }

  async findAll(): Promise<Array<TimeEntryResponseDto> | null> {
    const timeEntries = await this.prisma.timeEntry.findMany();
    return timeEntries.map((t) => this.convertToTimeEntryResponseDto(t));
  }

  async findOne(id: string): Promise<TimeEntryResponseDto | null> {
    const timeEntry = await this.prisma.timeEntry.findUnique({ where: { id } });
    return this.convertToTimeEntryResponseDto(timeEntry);
  }

  async update(
    id: string,
    updateTimeEntryDto: UpdateTimeEntryDto,
  ): Promise<TimeEntryResponseDto | null> {
    const timeEntry = await this.prisma.timeEntry.update({
      where: { id },
      data: updateTimeEntryDto,
    });
    return this.convertToTimeEntryResponseDto(timeEntry);
  }

  async remove(id: string): Promise<TimeEntryResponseDto | null> {
    const timeEntry = await this.prisma.timeEntry.delete({ where: { id } });
    return this.convertToTimeEntryResponseDto(timeEntry);
  }

  async findOneWithTags(id: string) {
    const timeEntry = await this.prisma.timeEntry.findUnique({
      where: { id },
      include: {
        tags: { select: { tag: { select: { id: true, name: true } } } },
      },
    });

    return timeEntry;
  }
}
