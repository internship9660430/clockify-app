import { Injectable } from '@nestjs/common';
import { CreateProfileDto } from 'src/common/dto/profile/create-profile.dto';
import { ProfileResponseDto } from 'src/common/dto/profile/profile-response.dto';
import { UpdateProfileDto } from 'src/common/dto/profile/update-profile.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { ProfileEntity } from './entities/profile/profile.entity';

@Injectable()
export class ProfileDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToProfileResponseDto(
    profileEntity: ProfileEntity,
  ): ProfileResponseDto {
    const profileResponse: ProfileResponseDto = {
      id: profileEntity.id,
      name: profileEntity.name,
      photoLocation: profileEntity.photoLocation,
      language: profileEntity.language,
      timeZone: profileEntity.timeZone,
      userId: profileEntity.userId,
    };

    return profileResponse;
  }

  async create(
    createProfileDto: CreateProfileDto,
  ): Promise<ProfileResponseDto | null> {
    const profile = await this.prisma.profile.create({
      data: createProfileDto,
    });
    return this.convertToProfileResponseDto(profile);
  }

  async findAll(): Promise<Array<ProfileResponseDto> | null> {
    const profiles = await this.prisma.profile.findMany();
    return profiles.map((p) => this.convertToProfileResponseDto(p));
  }

  async findOne(id: string): Promise<ProfileResponseDto | null> {
    const profile = await this.prisma.profile.findUnique({ where: { id } });
    return this.convertToProfileResponseDto(profile);
  }

  async update(
    id: string,
    updateProfileDto: UpdateProfileDto,
  ): Promise<ProfileResponseDto | null> {
    const profile = await this.prisma.profile.update({
      where: { id },
      data: updateProfileDto,
    });
    return this.convertToProfileResponseDto(profile);
  }

  async remove(id: string): Promise<ProfileResponseDto | null> {
    const profile = await this.prisma.profile.delete({ where: { id } });
    return this.convertToProfileResponseDto(profile);
  }
}
