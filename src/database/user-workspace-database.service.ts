import { UpdateUserWorkspaceDto } from './../common/dto/workspace/update-user-workspace.dto';
import { CreateUserWorkspaceDto } from './../common/dto/workspace/create-user-workspace.dto';
import { Injectable } from '@nestjs/common';
import { Role } from '@prisma/client';
import { WorkspaceUsersDto } from 'src/common/dto/workspace/workspace-users.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class UserWorkspaceDatabaseService {
  constructor(private prisma: PrismaService) {}

  async create(createUserWorkspaceDto: CreateUserWorkspaceDto) {
    await this.prisma.userWorkspace.create({ data: createUserWorkspaceDto });
  }

  async findWithUserAndWorkspaceIds(userId: string, workspaceId: string) {
    return await this.prisma.userWorkspace.findFirst({
      where: { userId, workspaceId },
    });
  }

  async update(id: string, updateUserWorkspaceDto: UpdateUserWorkspaceDto) {
    await this.prisma.userWorkspace.update({
      where: { id },
      data: updateUserWorkspaceDto,
    });
  }

  async removeAdmins(
    workspaceId: string,
    workspaceUsersDto: WorkspaceUsersDto,
  ) {
    const promises = workspaceUsersDto.userIds.map(async (userId) => {
      return await this.prisma.userWorkspace.updateMany({
        where: { workspaceId, userId },
        data: { role: Role.MEMBER },
      });
    });

    await Promise.all(promises);
  }

  async removeMember(workspaceId: string, userId: string) {
    await this.prisma.userWorkspace.deleteMany({
      where: { userId, workspaceId },
    });
  }
}
