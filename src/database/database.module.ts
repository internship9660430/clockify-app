import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UserDatabaseService } from './user-database.service';
import { ProfileDatabaseService } from './profile-database.service';
import { WorkspaceDatabaseService } from './workspace-database.service';
import { TimeEntryDatabaseService } from './time-entry-database.service';
import { ProjectDatabaseService } from './project-database.service';
import { TaskDatabaseService } from './task-database.service';
import { ClientDatabaseService } from './client-database.service';
import { TagDatabaseService } from './tag-database.service';
import { UserWorkspaceDatabaseService } from './user-workspace-database.service';
import { EntryTagDatabaseService } from './entry-tag-database.service';

@Module({
  imports: [PrismaModule],
  providers: [
    UserDatabaseService,
    ProfileDatabaseService,
    WorkspaceDatabaseService,
    UserWorkspaceDatabaseService,
    TimeEntryDatabaseService,
    ProjectDatabaseService,
    TaskDatabaseService,
    ClientDatabaseService,
    TagDatabaseService,
    EntryTagDatabaseService,
  ],
  exports: [
    UserDatabaseService,
    ProfileDatabaseService,
    WorkspaceDatabaseService,
    UserWorkspaceDatabaseService,
    TimeEntryDatabaseService,
    ProjectDatabaseService,
    TaskDatabaseService,
    ClientDatabaseService,
    TagDatabaseService,
    EntryTagDatabaseService,
  ],
})
export class DatabaseModule {}
