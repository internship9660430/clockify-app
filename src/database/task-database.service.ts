import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from '../common/dto/task/create-task.dto';
import { UpdateTaskDto } from '../common/dto/task/update-task.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { TaskEntity } from './entities/task/task.entity';
import { TaskResponseDto } from 'src/common/dto/task/task-response.dto';

@Injectable()
export class TaskDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToTaskResponseDto(taskEntity: TaskEntity): TaskResponseDto {
    const taskResponse: TaskResponseDto = {
      id: taskEntity.id,
      name: taskEntity.name,
      projectId: taskEntity.projectId,
    };

    return taskResponse;
  }

  async create(createTaskDto: CreateTaskDto): Promise<TaskResponseDto | null> {
    const task = await this.prisma.task.create({ data: createTaskDto });
    return this.convertToTaskResponseDto(task);
  }

  async findAll(): Promise<Array<TaskResponseDto> | null> {
    const tasks = await this.prisma.task.findMany();
    return tasks.map((t) => this.convertToTaskResponseDto(t));
  }

  async findOne(id: string): Promise<TaskResponseDto | null> {
    const task = await this.prisma.task.findUnique({ where: { id } });
    return this.convertToTaskResponseDto(task);
  }

  async update(
    id: string,
    updateTaskDto: UpdateTaskDto,
  ): Promise<TaskResponseDto | null> {
    const task = await this.prisma.task.update({
      where: { id },
      data: updateTaskDto,
    });
    return this.convertToTaskResponseDto(task);
  }

  async remove(id: string): Promise<TaskResponseDto | null> {
    const task = await this.prisma.task.delete({ where: { id } });
    return this.convertToTaskResponseDto(task);
  }
}
