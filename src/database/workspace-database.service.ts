import { WorkspaceEntity } from './entities/workspace/workspace.entity';
import { Injectable } from '@nestjs/common';
import { CreateWorkspaceDto } from '../common/dto/workspace/create-workspace.dto';
import { UpdateWorkspaceDto } from '../common/dto/workspace/update-workspace.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { WorkspaceResponseDto } from 'src/common/dto/workspace/workspace-response.dto';
import { Role } from '@prisma/client';

@Injectable()
export class WorkspaceDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToWorkspaceResponseDto(
    workspaceEntity: WorkspaceEntity,
  ): WorkspaceResponseDto {
    const workspaceResponse: WorkspaceResponseDto = {
      id: workspaceEntity.id,
      name: workspaceEntity.name,
    };

    return workspaceResponse;
  }

  async create(
    createWorkspaceDto: CreateWorkspaceDto,
  ): Promise<WorkspaceResponseDto | null> {
    const { ownerId, ...workspaceInfo } = createWorkspaceDto;

    const workspace = await this.prisma.workspace.create({
      data: workspaceInfo,
    });

    await this.prisma.userWorkspace.create({
      data: {
        userId: ownerId,
        workspaceId: workspace.id,
        role: Role.OWNER,
      },
    });

    return this.convertToWorkspaceResponseDto(workspace);
  }

  async findAll(): Promise<Array<WorkspaceResponseDto> | null> {
    const workspaces = await this.prisma.workspace.findMany();
    return workspaces.map((w) => this.convertToWorkspaceResponseDto(w));
  }

  async findOne(id: string): Promise<WorkspaceResponseDto | null> {
    const workspace = await this.prisma.workspace.findUnique({ where: { id } });
    return this.convertToWorkspaceResponseDto(workspace);
  }

  async update(
    id: string,
    updateWorkspaceDto: UpdateWorkspaceDto,
  ): Promise<WorkspaceResponseDto | null> {
    const workspace = await this.prisma.workspace.update({
      where: { id },
      data: updateWorkspaceDto,
    });
    return this.convertToWorkspaceResponseDto(workspace);
  }

  async remove(id: string): Promise<WorkspaceResponseDto | null> {
    const workspace = await this.prisma.workspace.delete({ where: { id } });
    return this.convertToWorkspaceResponseDto(workspace);
  }

  async findOneWithOwner(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        users: {
          where: { role: Role.OWNER },
          select: { user: true, role: true },
        },
      },
    });

    return workspace;
  }

  async findOneWithAdmins(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        users: {
          where: { role: Role.ADMIN },
          select: { user: true, role: true },
        },
      },
    });

    return workspace;
  }

  async findOneWithMembers(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        users: {
          where: { role: Role.MEMBER },
          select: { user: true, role: true },
        },
      },
    });

    return workspace;
  }

  async findOneWithUsers(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        users: {
          select: { user: true, role: true },
        },
      },
    });

    return workspace;
  }

  async findOneWithUserTimeEntries(id: string, userId: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        timeEntries: {
          where: { userId },
        },
      },
    });

    return workspace;
  }

  async findOneWithTimeEntries(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        timeEntries: true,
      },
    });

    return workspace;
  }

  async findOneWithProjects(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        projects: true,
      },
    });

    return workspace;
  }

  async findOneWithClients(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        clients: true,
      },
    });

    return workspace;
  }

  async findOneWithTags(id: string) {
    const workspace = await this.prisma.workspace.findUnique({
      where: { id },
      include: {
        tags: true,
      },
    });

    return workspace;
  }
}
