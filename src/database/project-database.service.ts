import { Injectable } from '@nestjs/common';
import { CreateProjectDto } from '../common/dto/project/create-project.dto';
import { UpdateProjectDto } from '../common/dto/project/update-project.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { ProjectEntity } from './entities/project/project.entity';
import { ProjectResponseDto } from 'src/common/dto/project/project-response.dto';

@Injectable()
export class ProjectDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToProjectResponseDto(
    projectEntity: ProjectEntity,
  ): ProjectResponseDto {
    const projectResponse: ProjectResponseDto = {
      id: projectEntity.id,
      name: projectEntity.name,
      color: projectEntity.color,
      workspaceId: projectEntity.workspaceId,
      clientId: projectEntity.clientId,
    };

    return projectResponse;
  }

  async create(
    createProjectDto: CreateProjectDto,
  ): Promise<ProjectResponseDto | null> {
    const project = await this.prisma.project.create({
      data: createProjectDto,
    });
    return this.convertToProjectResponseDto(project);
  }

  async findAll(): Promise<Array<ProjectResponseDto> | null> {
    const projects = await this.prisma.project.findMany();
    return projects.map((p) => this.convertToProjectResponseDto(p));
  }

  async findOne(id: string): Promise<ProjectResponseDto | null> {
    const project = await this.prisma.project.findUnique({ where: { id } });
    return this.convertToProjectResponseDto(project);
  }

  async update(
    id: string,
    updateProjectDto: UpdateProjectDto,
  ): Promise<ProjectResponseDto | null> {
    const project = await this.prisma.project.update({
      where: { id },
      data: updateProjectDto,
    });
    return this.convertToProjectResponseDto(project);
  }

  async remove(id: string): Promise<ProjectResponseDto | null> {
    const project = await this.prisma.project.delete({ where: { id } });
    return this.convertToProjectResponseDto(project);
  }

  async findOneWithTasks(id: string) {
    const project = await this.prisma.project.findUnique({
      where: { id },
      include: { tasks: true },
    });

    return project;
  }
}
