import { Injectable } from '@nestjs/common';
import { CreateEntryTagDto } from 'src/common/dto/time-entry/create-entry-tag.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class EntryTagDatabaseService {
  constructor(private prisma: PrismaService) {}

  async create(createEntryTagDto: CreateEntryTagDto) {
    await this.prisma.entryTag.create({ data: createEntryTagDto });
  }

  async remove(timeEntryId: string, tagId: string) {
    await this.prisma.entryTag.deleteMany({
      where: {
        timeEntryId,
        tagId,
      },
    });
  }
}
