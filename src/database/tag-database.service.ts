import { Injectable } from '@nestjs/common';
import { CreateTagDto } from '../common/dto/tag/create-tag.dto';
import { UpdateTagDto } from '../common/dto/tag/update-tag.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { TagEntity } from './entities/tag/tag.entity';
import { TagResponseDto } from 'src/common/dto/tag/tag-response.dto';

@Injectable()
export class TagDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToTagResponseDto(tagEntity: TagEntity): TagResponseDto {
    const tagResponse: TagResponseDto = {
      id: tagEntity.id,
      name: tagEntity.name,
      workspaceId: tagEntity.workspaceId,
    };

    return tagResponse;
  }

  async create(createTagDto: CreateTagDto): Promise<TagResponseDto | null> {
    const tag = await this.prisma.tag.create({ data: createTagDto });
    return this.convertToTagResponseDto(tag);
  }

  async findAll(): Promise<Array<TagResponseDto> | null> {
    const tag = await this.prisma.tag.findMany();
    return tag.map((t) => this.convertToTagResponseDto(t));
  }

  async findOne(id: string): Promise<TagResponseDto | null> {
    const tag = await this.prisma.tag.findUnique({ where: { id } });
    return this.convertToTagResponseDto(tag);
  }

  async update(
    id: string,
    updateTagDto: UpdateTagDto,
  ): Promise<TagResponseDto | null> {
    const tag = await this.prisma.tag.update({
      where: { id },
      data: updateTagDto,
    });
    return this.convertToTagResponseDto(tag);
  }

  async remove(id: string): Promise<TagResponseDto | null> {
    const tag = await this.prisma.tag.delete({ where: { id } });
    return this.convertToTagResponseDto(tag);
  }
}
