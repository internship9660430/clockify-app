import { Injectable } from '@nestjs/common';
import { CreateUserDto } from 'src/common/dto/user/create-user.dto';
import { UpdateUserDto } from 'src/common/dto/user/update-user.dto';
import { UserResponseDto } from 'src/common/dto/user/user-response.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { UserEntity } from './entities/user/user.entity';
import { UserResponseWithProfileDto } from 'src/common/dto/user/user-response-with-profile.dto';
import { Role } from '@prisma/client';

@Injectable()
export class UserDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToUserResponseDto(userEntity: UserEntity): UserResponseDto {
    const userResponse: UserResponseDto = {
      id: userEntity.id,
      email: userEntity.email,
      password: userEntity.password,
    };

    return userResponse;
  }

  async create(createUserDto: CreateUserDto): Promise<UserResponseDto | null> {
    const user = await this.prisma.user.create({ data: createUserDto });
    return this.convertToUserResponseDto(user);
  }

  async findAll(): Promise<Array<UserResponseDto> | null> {
    const users = await this.prisma.user.findMany();
    return users.map((u) => this.convertToUserResponseDto(u));
  }

  async findAllWithEmail(
    email: string,
  ): Promise<Array<UserResponseDto> | null> {
    const users = await this.prisma.user.findMany({ where: { email } });
    return users.map((u) => this.convertToUserResponseDto(u));
  }

  async findOne(id: string): Promise<UserResponseDto | null> {
    const user = await this.prisma.user.findUnique({ where: { id } });
    return this.convertToUserResponseDto(user);
  }

  async update(
    id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<UserResponseDto | null> {
    const user = await this.prisma.user.update({
      where: { id },
      data: updateUserDto,
    });
    return this.convertToUserResponseDto(user);
  }

  async remove(id: string): Promise<UserResponseDto | null> {
    await this.prisma.workspace.deleteMany({
      where: {
        users: {
          every: {
            userId: id,
            role: Role.OWNER,
          },
        },
      },
    });

    const user = await this.prisma.user.delete({ where: { id } });
    return this.convertToUserResponseDto(user);
  }

  async findOneWithProfile(id: string) {
    const user = await this.prisma.user.findUnique({
      where: { id },
      include: {
        profile: true,
      },
    });

    const userResponse: UserResponseWithProfileDto = {
      id: user.id,
      email: user.email,
      password: user.password,
      profile: {
        id: user.profile.id,
        name: user.profile.name,
        photoLocation: user.profile.photoLocation,
        language: user.profile.language,
        timeZone: user.profile.timeZone,
      },
    };

    return userResponse;
  }

  async findOneWithWorkspaces(id: string) {
    const user = await this.prisma.user.findUnique({
      where: { id },
      include: {
        workspaces: { select: { workspace: true, role: true } },
      },
    });

    return user;
  }
}
