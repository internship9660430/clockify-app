import { Injectable } from '@nestjs/common';
import { CreateClientDto } from 'src/common/dto/client/create-client.dto';
import { UpdateClientDto } from 'src/common/dto/client/update-client.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { ClientEntity } from './entities/client/client.entity';
import { ClientResponseDto } from 'src/common/dto/client/client-response.dto';

@Injectable()
export class ClientDatabaseService {
  constructor(private prisma: PrismaService) {}

  convertToClientResponseDto(clientEntity: ClientEntity): ClientResponseDto {
    const clientResponse: ClientResponseDto = {
      id: clientEntity.id,
      name: clientEntity.name,
      workspaceId: clientEntity.workspaceId,
    };

    return clientResponse;
  }

  async create(
    createClientDto: CreateClientDto,
  ): Promise<ClientResponseDto | null> {
    const client = await this.prisma.client.create({ data: createClientDto });
    return this.convertToClientResponseDto(client);
  }

  async findAll(): Promise<Array<ClientResponseDto> | null> {
    const clients = await this.prisma.client.findMany();
    return clients.map((c) => this.convertToClientResponseDto(c));
  }

  async findOne(id: string): Promise<ClientResponseDto | null> {
    const client = await this.prisma.client.findUnique({ where: { id } });
    return this.convertToClientResponseDto(client);
  }

  async update(
    id: string,
    updateClientDto: UpdateClientDto,
  ): Promise<ClientResponseDto | null> {
    const client = await this.prisma.client.update({
      where: { id },
      data: updateClientDto,
    });
    return this.convertToClientResponseDto(client);
  }

  async remove(id: string): Promise<ClientResponseDto | null> {
    const client = await this.prisma.client.delete({ where: { id } });
    return this.convertToClientResponseDto(client);
  }
}
