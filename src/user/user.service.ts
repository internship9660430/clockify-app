import { UserDatabaseService } from '../database/user-database.service';
import { Injectable } from '@nestjs/common';
import { CreateUserDto } from '../common/dto/user/create-user.dto';
import { UpdateUserDto } from '../common/dto/user/update-user.dto';

@Injectable()
export class UserService {
  constructor(private readonly userDatabaseService: UserDatabaseService) {}

  create(createUserDto: CreateUserDto) {
    return this.userDatabaseService.create(createUserDto);
  }

  findAll(email: string) {
    if (email) {
      return this.userDatabaseService.findAllWithEmail(email);
    }
    return this.userDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.userDatabaseService.findOne(id);
  }

  update(id: string, updateUserDto: UpdateUserDto) {
    return this.userDatabaseService.update(id, updateUserDto);
  }

  remove(id: string) {
    return this.userDatabaseService.remove(id);
  }

  findOneWithProfile(id: string) {
    return this.userDatabaseService.findOneWithProfile(id);
  }

  findOneWithWorkspaces(id: string) {
    return this.userDatabaseService.findOneWithWorkspaces(id);
  }
}
