import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from '../common/dto/user/create-user.dto';
import { UpdateUserDto } from '../common/dto/user/update-user.dto';
import {
  ApiCreatedResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { UserResponseDto } from 'src/common/dto/user/user-response.dto';
import { UserResponseWithProfileDto } from 'src/common/dto/user/user-response-with-profile.dto';
import { UserResponseWithWorkspacesDto } from 'src/common/dto/user/user-response-with-workspaces.dto';

@Controller('users')
@ApiTags('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiCreatedResponse({ type: UserResponseDto })
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @Get()
  @ApiOkResponse({ type: UserResponseDto, isArray: true })
  @ApiQuery({ name: 'email', required: false })
  findAll(@Query('email') email?: string) {
    return this.userService.findAll(email);
  }

  @Get(':id')
  @ApiOkResponse({ type: UserResponseDto })
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: UserResponseDto })
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: UserResponseDto })
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }

  @Get(':id/profile')
  @ApiOkResponse({ type: UserResponseWithProfileDto })
  findOneWithProfile(@Param('id') id: string) {
    return this.userService.findOneWithProfile(id);
  }

  @Get(':id/workspaces')
  @ApiOkResponse({ type: UserResponseWithWorkspacesDto })
  findOneWithWorkspaces(@Param('id') id: string) {
    return this.userService.findOneWithWorkspaces(id);
  }
}
