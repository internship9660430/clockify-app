import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ProjectService } from './project.service';
import { CreateProjectDto } from '../common/dto/project/create-project.dto';
import { UpdateProjectDto } from '../common/dto/project/update-project.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ProjectResponseDto } from 'src/common/dto/project/project-response.dto';
import { ProjectResponseWithTasksDto } from 'src/common/dto/project/project-response-with-tasks.dto';

@Controller('projects')
@ApiTags('projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Post()
  @ApiCreatedResponse({ type: ProjectResponseDto })
  create(@Body() createProjectDto: CreateProjectDto) {
    return this.projectService.create(createProjectDto);
  }

  @Get()
  @ApiOkResponse({ type: ProjectResponseDto, isArray: true })
  findAll() {
    return this.projectService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: ProjectResponseDto })
  findOne(@Param('id') id: string) {
    return this.projectService.findOne(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: ProjectResponseDto })
  update(@Param('id') id: string, @Body() updateProjectDto: UpdateProjectDto) {
    return this.projectService.update(id, updateProjectDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: ProjectResponseDto })
  remove(@Param('id') id: string) {
    return this.projectService.remove(id);
  }

  @Get(':id/tasks')
  @ApiOkResponse({ type: ProjectResponseWithTasksDto })
  findOneWithTasks(@Param('id') id: string) {
    return this.projectService.findOneWithTasks(id);
  }
}
