import { Injectable } from '@nestjs/common';
import { CreateProjectDto } from '../common/dto/project/create-project.dto';
import { UpdateProjectDto } from '../common/dto/project/update-project.dto';
import { ProjectDatabaseService } from 'src/database/project-database.service';

@Injectable()
export class ProjectService {
  constructor(
    private readonly projectDatabaseService: ProjectDatabaseService,
  ) {}

  create(createProjectDto: CreateProjectDto) {
    return this.projectDatabaseService.create(createProjectDto);
  }

  findAll() {
    return this.projectDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.projectDatabaseService.findOne(id);
  }

  update(id: string, updateProjectDto: UpdateProjectDto) {
    return this.projectDatabaseService.update(id, updateProjectDto);
  }

  remove(id: string) {
    return this.projectDatabaseService.remove(id);
  }

  findOneWithTasks(id: string) {
    return this.projectDatabaseService.findOneWithTasks(id);
  }
}
