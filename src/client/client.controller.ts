import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ClientService } from './client.service';
import { CreateClientDto } from 'src/common/dto/client/create-client.dto';
import { UpdateClientDto } from 'src/common/dto/client/update-client.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ClientResponseDto } from 'src/common/dto/client/client-response.dto';

@Controller('clients')
@ApiTags('clients')
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Post()
  @ApiCreatedResponse({ type: ClientResponseDto })
  create(@Body() createClientDto: CreateClientDto) {
    return this.clientService.create(createClientDto);
  }

  @Get()
  @ApiOkResponse({ type: ClientResponseDto, isArray: true })
  findAll() {
    return this.clientService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ type: ClientResponseDto })
  findOne(@Param('id') id: string) {
    return this.clientService.findOne(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: ClientResponseDto })
  update(@Param('id') id: string, @Body() updateClientDto: UpdateClientDto) {
    return this.clientService.update(id, updateClientDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: ClientResponseDto })
  remove(@Param('id') id: string) {
    return this.clientService.remove(id);
  }
}
