import { Injectable } from '@nestjs/common';
import { CreateClientDto } from 'src/common/dto/client/create-client.dto';
import { UpdateClientDto } from 'src/common/dto/client/update-client.dto';
import { ClientDatabaseService } from 'src/database/client-database.service';

@Injectable()
export class ClientService {
  constructor(private readonly clientDatabaseService: ClientDatabaseService) {}

  create(createClientDto: CreateClientDto) {
    return this.clientDatabaseService.create(createClientDto);
  }

  findAll() {
    return this.clientDatabaseService.findAll();
  }

  findOne(id: string) {
    return this.clientDatabaseService.findOne(id);
  }

  update(id: string, updateClientDto: UpdateClientDto) {
    return this.clientDatabaseService.update(id, updateClientDto);
  }

  remove(id: string) {
    return this.clientDatabaseService.remove(id);
  }
}
